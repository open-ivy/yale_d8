<?php

/**
 * @file
 * Functionality for the yale_ct_alert module.
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\node\Entity\Node;

/**
 * Implements hook_entity_insert().
 */
function yale_ct_alert_entity_insert(EntityInterface $entity) {

  if ($entity instanceof Node && $entity->getType() == 'alert' && $entity->get('status')->value == NODE_PUBLISHED) {
    // Unpublish any other alerts.
    $otherAlertsNids = \Drupal::entityQuery('node')
      ->condition('type', 'alert')
      ->condition('status', NODE_PUBLISHED)
      ->condition('nid', $entity->id(), '<>')
      ->execute();

    /** @var \Drupal\node\Entity\Node[] $otherAlerts */
    $otherAlerts = Node::loadMultiple($otherAlertsNids);
    foreach ($otherAlerts as $otherAlert) {
      $otherAlert
        ->set('status', NODE_NOT_PUBLISHED)
        ->save();
    }
  }
}

/**
 * Implements hook_entity_update().
 */
function yale_ct_alert_entity_update(EntityInterface $entity) {

  if ($entity instanceof Node && $entity->getType() == 'alert' && $entity->get('status')->value == NODE_PUBLISHED && $entity->original->get('status')->value == NODE_NOT_PUBLISHED) {
    // Unpublish any other alerts.
    $otherAlertsNids = \Drupal::entityQuery('node')
      ->condition('type', 'alert')
      ->condition('status', NODE_PUBLISHED)
      ->condition('nid', $entity->id(), '<>')
      ->execute();

    /** @var \Drupal\node\Entity\Node[] $otherAlerts */
    $otherAlerts = Node::loadMultiple($otherAlertsNids);
    foreach ($otherAlerts as $otherAlert) {
      $otherAlert
        ->set('status', NODE_NOT_PUBLISHED)
        ->save();
    }
  }
}
