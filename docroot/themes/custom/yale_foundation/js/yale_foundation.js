/**
 * @file
 * Placeholder file for custom sub-theme behaviors.
 */

(function ($, Drupal) {

  /**
   * Use this behavior as a template for custom Javascript.
   */
  Drupal.behaviors.exampleBehavior = {
    attach: function (context, settings) {

    }
  };

  /**
   * Enable highlight.js on page load.
   */
  Drupal.behaviors.yaleHighlightjs = {
    attach: function (context, settings) {
      $(document).ready(function () {
        hljs.initHighlightingOnLoad();
      });
    }
  };

  /**
   * Custom scrolling functionality.
   *
   * This is not in a Drupal behavior, but an event handler instead to allow for any link to utilize this.
   */
  $(document).on('click', 'a[href^=#]', function () {
    var target = $(this).attr('href');
    // Ignore anchors linking to themselves.
    if (target === '#') {
      return;
    }

    // Ignore anchors linking to invisible elements.
    var $target = $(target);
    if ($target.length === 0) {
      return;
    }

    // Give focus to the current element.
    $(this).focus();

    // If we find the element, scroll. If not, return usual behaviour.
    var targetPosition = $target.offset();
    if (targetPosition.top) {
      $('html, body')
          .animate({scrollTop: targetPosition.top}, 600)
          .promise()
          .then(function () {
            $target.focus();
          });
    }
  });

  // Since menu--off-canvas.html.twig is used for both instances of the menu,
  // behaviour has to be applied via JS instead of data attributes.
  var mainMenu = $('.block-main-menu .menu')
    .addClass('dropdown');
  new Foundation.DropdownMenu(mainMenu, {});

  var mainMenuMobile = $('.off-canvas .menu')
    .addClass('vertical');
  new Foundation.AccordionMenu(mainMenuMobile, {});

})(jQuery, Drupal);
