Yale University

Drupal Architecture
========================
v1.0

Note: This is a living document that will evolve throughout the lifecycle of the project. Any delivery of this document is a snapshot in time and may be either incomplete or inaccurate as the project/platform evolves. This and related documents will be maintained throughout the project and the goal is to most accurately reflect the current known state of architecture for the various project components.

## Contents

- [Background](#background)
  - [Discovery](#discovery)
	- [Project Methodology](#project-methodology)
	- [Design](#design)
- [Technical Overview](#technical-overview)
  - [Infrastructure](#infrastructure)
      - [Environments](#environments)
      - [Traffic Management and CDN](#traffic-management-and-cdn)
  - [High-level Technical Requirements](#high-level-technical-requirements)
    - [Security](#security)
    - [Performance](#performance)
      - [Content Caching](#content-caching)
  - [Recommended practices](#recommended-practices)
    - [Performance profiling and testing](#performance-profiling-and-testing)
    - [Performance Configuration Review](#performance-configuration-review)
    - [Performance Testing](#performance-testing)
    - [Load and Stress testing](#load-and-stress-testing)
    - [Performance Goals](#performance-goals)
- [Development and deployment strategy](#development-and-deployment-strategy)
   - [Testing](#testing)
      - [QA Testing](#qa-testing)
     - [Performance and Load Testing](#performance-load)
- [User roles](#user-roles)
- [Custom and contributed projects](#custom-and-contributed-projects)
  - [Install Profile](#install-profile)
  - [Contributed Modules](#contributed-modules)
  - [Custom Modules](#custom-modules)
  - [Base Theme](#base-theme)
  - [Custom Themes](#custom-themes)
  - [Libraries](#libraries)
- [Content Architecture](#content-architecture)
  - [Article (content type)](#article)
   - [Features](#features)
   - [Assumptions](#assumptions)
   - [Risks](#risks)
   - [Examples](#examples)
  - [Tags (vocabulary)](#tags-vocabulary)
- [Features](#features)
  - [Feature A](#feature-a)
   - [Assumptions](#assumptions)
   - [Risks](#risks)
- [Theme Architecture](#theme-architecture)
- [Integrations Summary](#integrations-summary)
- [Migration](#migration)
   - [Assumptions](#assumptions)
   - [Risks](#risks)



#Background

##Discovery
Since its founding in 1701, Yale has been dedicated to expanding and sharing knowledge, inspiring innovation, and preserving cultural and scientific information for future generations.

Yale’s reach is both local and international. It partners with its hometown of New Haven, Connecticut to strengthen the city’s community and economy. And it engages with people and institutions across the globe in the quest to promote cultural understanding, improve the human condition, delve more deeply into the secrets of the universe, and train the next generation of world leaders.

Yale's long term goal is to move Yale off of their current infrastructure onto Acquia cloud and also to migrate the 1500 sites that they have from Drupal 6 and Drupal 7 to Drupal 8. The goal of this project is to build a foundation in Drupal 8 and also to enable Yale to be able to enhance and support the platform in the future.

##Project methodology
The project should consist of bi-weekly sprints that address a body of work that has been approved by Yale.  Each sprint will contain the amount of work that the team feels they can easily complete.  Completion of work includes quality assurance, user acceptance testings, and deployment to Acquia Cloud.  Before each sprint, tickets should be groomed to a state where developers can easily understand the work at hand.

##Design
Yale University utilizes a common brand/design identity across the entire university.  In order to achieve this, Yale has provided open tools to automatically style and theme sites according to their brand.  We will implement these tools into the Drupal 8 site.  Tools can be seen here: [YaleSites](http://yalesites.yale.edu/)


#Technical overview
This project is based off of multiple projects:

* [Acquia Bolt](https://github.com/acquia/blt)
* [Acquia Lightning](https://www.drupal.org/project/lightning)
* [Zurb Foundation](https://www.drupal.org/project/zurb_foundation)
* [Drupal 8](https://www.drupal.org/8)
* [YaleSites](http://yalesites.yale.edu/)

All of the open source projects included in the build create the Yale Drupal 8 (D8) Distribution.

##Infrastructure
This distribution will power ~1500 sites on Acquia Cloud.  Each site will have multiple environments listed below.

###Environments

| Environment | Short name | Audience                | Purpose                                |
|-------------|------------|-------------------------|----------------------------------------|
| Production  | prod       | Public                  | Public traffic                         |
| Staging     | test       | Client, Acquia, Partner | UAT                                    |
| Development | dev        | Acquia, Partner         | Code integration and developer testing |

### CDN
Currently, the sites re-purpose the [YaleSites](http://yalesites.yale.edu/) branch via native CSS/JS files that are dynamically downloaded during the build process.  One goal is to allow for a CDN of these assets that can easily power all ~1500 of Yale's sites outside of the Drupal system (with caching of assets in Drupal's filesystem).

##High-level technical requirements
| Business Objective | Technical Requirement | 
|-------------|------------|
| Lowering costs of hosting | Moving sites off of current platform and onto Acquia Cloud. | 
| Decrease Deployment and maintenance efforts | Improve deployment and maintenance processes. |
| Site Experience Consistency | Better consistency across sites by moving existing sites from D6 to D8. |
| Better approach for moving to D8 | Move to D8 in a way that is reproducible. When going from D8 to D9, it should be very simple. |
| Better support for Accessibility | There is no support for accessibility on D6 sites and minimal support on D7 sites. Accessibility should be built in to D8 platform. | 
| Sharing of Content | Enable the sharing of content across different platforms in the future. |

###Performance
####Content Caching
The site utilizes Drupal 8's Cache API which is much improved from Drupal 7.  All things that either are directly renderable or are used to determine what to render provide cacheability metadata.  

##Recommended practices
####Performance Configuration Review
At a high-level, there are many Drupal configuration options that can affect overall site performance. These settings are likely environment-specific and should be verified only against the production configuration, not local developer environments. Built-in and custom Insight tests can be used to track this data. Automated tools like Cache Audit can also be used locally for reviewing.

Examples include:

- Drupal performance settings (e.g. page cache lifetime, block caching, CSS/JS aggregation)
- Views cache settings (plugin, query, output)
- Panels display/pane cache settings (plugin, output)


####Performance Testing
The platform should be profiled and performance tested during development. This should include XHProf / New Relic code profiling, as well as site configuration and code review for potential issues. Load testing tools like JMeter can be used to simulate traffic to create results, but should not be relied upon to show any scalability issues at this point.

Because platforms are largely composed of sets of independent components, the combination available for building pages is extensive. This makes profiling of full pages largely unpredictable as pages are built and creates a need to identify performance levels on a component by component basis. Each component should be classified by the following characteristics:

- Baseline processing time, generated from the platform demo site
- Functional complexity
    - "Static" - renders without the need to gather external content
    - "Collection" - shows content external to the component, but defined by a specific list (rather than a query); for example, a list of one or many entity references
    - "Dynamic" - uses DB queries/cache requests to determine which content to show
    - "External" - uses an external web service, including Solr, to gather and show content
- Cache interval
    - "Never" - a new copy of the content is rendered within each request
    - "Time" - after a specific period of time, the content must be regenerated
		- "Content" - cache clearing responds to a specific event that is outside of this component


####Load and stress testing
Prior to public launch, the site(s) should be load and stress tested on a suitable load test environment, ideally one which mirrors the production environment, to provide indications of bottlenecks.  Load and stress test need to occur on a timetable of no less than 2-3 weeks prior to launch in order to allow proper remediation of bottlenecks and tuning of hosting environments.

**The base platform should include a basic JMeter performance testing script which can be augmented with site-specific URLs (if relevant) and editorial process for authenticated traffic.** If further modes of testing are required (such as full browser emulation), additional tools or vendors may be required. If extensive AJAX / JS-related functionality exists, "real user" load testing is recommended over request-based tools like JMeter.

*Test scenarios should be made in collaboration with the client.* Using previous traffic analysis, typical user paths should be used for testing rather than a simple list of URLs. This more closely mimics real user traffic and also helps identify specific actions that may be taken, such as filling out forms or other interactions. It also better isolates where problems may exist and allow priorities to be made following test analysis.

The platform as a whole will be tested against total anticipated traffic numbers. Each site will require a new set of scenarios that will be run prior to launch and at each new platform launch. For example, in a multisite configuration, a launch of 4 sites will require 4 sets of scenarios and have the total traffic spread across 4 sites. At the next launch of 4 additional sites, 4 additional sets of scenarios will be created and the total load will be spread across all 8 existing sites.
Performance Goals
In order to quantify the pass / fail criteria for testing, specific goals must be established. These goals are not meant as a requirement of the platform, but will guide the pace at which new features are added if performance slows during development. The following performance metrics are the target for the platform:

- Total page views (per day/week/month)
   - 20 page views per hour, 50 peak
	 - 500 page views per day, 700 peak
- Average page load times (average / max / min)
- Number of concurrent users (max / min / average)
- Response time (time to first and last bytes)
- Requests per second (average / min / max)
- Editor experience: average / min / max time it takes to save an Article into the current CMS once the submit button is pressed.

#Development and deployment strategy
Code is currently sourced in [Bitbucket](https://bitbucket.org/its_yale/yale_d8) and is hosting on [Acquia Cloud](https://insight.acquia.com/cloud/workflow?s=2666111), and provides tools to tightly integrate with any CI platform.  

Upon certain triggers, such as an approval of code during a review in bitbucket, code should deploy automatically to the [staged site on Acquia Cloud](https://insight.acquia.com/cloud/workflow?s=2666111).  Upon each build, code will be tested for it's cleanliness, structure, and functionality automatically using PHPUnit, PHP Code Sniffer, and Behat.  The code will also be tested for latest security updates from Drupal.org.  It's essentialy that every deployment occurs with the latest more secure code in the Drupal ecosystem.

#User roles
The default deployment of the site includes the following roles:

| Role name | Permissions (summary) |
| Administrator | 100% control over all aspects of the Drupal site. |
| Site Administrator | Percieved as 100% control over all aspects of the Drupal site, but is limited to only web admin rights. |
| Site Builder | Can build aspects of the site using Context, Panels, or other site building Drupal tools. |
| Editor | Can create, schedule, approve, and publish content. |
| Authenticated | Can login to the site. |
| Anonymous | Can view published content. |


#Custom and contributed projects
##Install profile
The site is based off of the [Acquia Lightning](https://www.drupal.org/project/lightning) profile.  Acquia lightning provides tools to extend it's distribution, and therefore we've enabled it to automatically build the site's custom functionality accordingly.

##Contributed modules
|*Package*|*Name*|*Status*|*Version*|
|------|--------------------------|--------|------|
| Acquia | Acquia connector (acquia_connector) | Not installed | 8.x-1.3 |
| Acquia | Acquia search (acquia_search) | Not installed | 8.x-1.3 |
| Acquia Cloud Site Factory | ACSF Connector (acsf) | Not installed | 8.x-1.26 |
| Acquia Cloud Site Factory | ACSF Duplication (acsf_duplication) | Not installed | 8.x-1.26 |
| Acquia Cloud Site Factory | ACSF Theme (acsf_theme) | Not installed | 8.x-1.26 |
| Acquia Cloud Site Factory | ACSF Variables (acsf_variables) | Not installed | 8.x-1.26 |
| Chaos tool suite | Chaos tools (ctools) | Enabled | 8.x-3.0-alpha26 |
| Chaos tool suite | Chaos tools Views (ctools_views) | Not installed | 8.x-3.0-alpha26 |
| Chaos tool suite (Experimental) | Chaos tools blocks (ctools_block) | Enabled | 8.x-3.0-alpha26 |
| Core | Actions (action) | Not installed | 8.1.7 |
| Core | Activity Tracker (tracker) | Not installed | 8.1.7 |
| Core | Aggregator (aggregator) | Not installed | 8.1.7 |
| Core | Automated Cron (automated_cron) | Not installed | 8.1.7 |
| Core | Ban (ban) | Not installed | 8.1.7 |
| Core | Block (block) | Enabled | 8.1.7 |
| Core | Book (book) | Not installed | 8.1.7 |
| Core | Breakpoint (breakpoint) | Enabled | 8.1.7 |
| Core | CKEditor (ckeditor) | Enabled | 8.1.7 |
| Core | Color (color) | Not installed | 8.1.7 |
| Core | Comment (comment) | Not installed | 8.1.7 |
| Core | Configuration Manager (config) | Enabled | 8.1.7 |
| Core | Contact (contact) | Enabled | 8.1.7 |
| Core | Contextual Links (contextual) | Enabled | 8.1.7 |
| Core | Custom Block (block_content) | Enabled | 8.1.7 |
| Core | Custom Menu Links (menu_link_content) | Enabled | 8.1.7 |
| Core | Database Logging (dblog) | Enabled | 8.1.7 |
| Core | Field (field) | Enabled | 8.1.7 |
| Core | Field UI (field_ui) | Enabled | 8.1.7 |
| Core | Filter (filter) | Enabled | 8.1.7 |
| Core | Forum (forum) | Not installed | 8.1.7 |
| Core | Help (help) | Enabled | 8.1.7 |
| Core | History (history) | Enabled | 8.1.7 |
| Core | Internal Dynamic Page Cache (dynamic_page_cache) | Not installed | 8.1.7 |
| Core | Internal Page Cache (page_cache) | Enabled | 8.1.7 |
| Core | Menu UI (menu_ui) | Enabled | 8.1.7 |
| Core | Node (node) | Enabled | 8.1.7 |
| Core | Path (path) | Enabled | 8.1.7 |
| Core | Quick Edit (quickedit) | Enabled | 8.1.7 |
| Core | RDF (rdf) | Enabled | 8.1.7 |
| Core | Responsive Image (responsive_image) | Not installed | 8.1.7 |
| Core | Search (search) | Enabled | 8.1.7 |
| Core | Shortcut (shortcut) | Enabled | 8.1.7 |
| Core | Statistics (statistics) | Not installed | 8.1.7 |
| Core | Syslog (syslog) | Not installed | 8.1.7 |
| Core | System (system) | Enabled | 8.1.7 |
| Core | Taxonomy (taxonomy) | Enabled | 8.1.7 |
| Core | Testing (simpletest) | Not installed | 8.1.7 |
| Core | Text Editor (editor) | Enabled | 8.1.7 |
| Core | Toolbar (toolbar) | Enabled | 8.1.7 |
| Core | Tour (tour) | Not installed | 8.1.7 |
| Core | Update Manager (update) | Enabled | 8.1.7 |
| Core | User (user) | Enabled | 8.1.7 |
| Core | Views (views) | Enabled | 8.1.7 |
| Core | Views UI (views_ui) | Enabled | 8.1.7 |
| Core (Experimental) | BigPipe (big_pipe) | Not installed | 8.1.7 |
| Core (Experimental) | Inline Form Errors (inline_form_errors) | Not installed | 8.1.7 |
| Core (Experimental) | Migrate (migrate) | Enabled | 8.1.7 |
| Core (Experimental) | Migrate Drupal (migrate_drupal) | Not installed | 8.1.7 |
| Core (Experimental) | Migrate Drupal UI (migrate_drupal_ui) | Not installed | 8.1.7 |
| Date/Time | Calendar (calendar) | Enabled | |
| Date/Time | Calendar Datetime (calendar_datetime) | Enabled | |
| Development | Features (features) | Enabled | 8.x-3.0-beta6 |
| Development | Features UI (features_ui) | Enabled | 8.x-3.0-beta6 |
| Examples | Block Page (with Layout plugin) (block_page_layout) | Not installed | 8.x-1.0-alpha22 |
| Examples | Example Layouts (layout_plugin_example) | Not installed | 8.x-1.0-alpha22 |
| Field types | Address (address) | Enabled | 8.x-1.0-beta3 |
| Field types | Datetime (datetime) | Enabled | 8.1.7 |
| Field types | Entity Reference Revisions (entity_reference_revisions) | Enabled | 8.x-1.0 |
| Field types | File (file) | Enabled | 8.1.7 |
| Field types | Image (image) | Enabled | 8.1.7 |
| Field types | Link (link) | Enabled | 8.1.7 |
| Field types | Options (options) | Enabled | 8.1.7 |
| Field types | Telephone (telephone) | Enabled | 8.1.7 |
| Field types | Text (text) | Enabled | 8.1.7 |
| Fields | Field Group (field_group) | Enabled | |
| Fields | Inline Entity Form (inline_entity_form) | Enabled | 8.x-1.0-alpha6 |
| Filters | Entity Embed (entity_embed) | Enabled | 8.x-1.0-alpha3 |
| Layout | Layout Plugin (layout_plugin) | Enabled | 8.x-1.0-alpha22 |
| Layout | Page Manager (page_manager) | Enabled | 8.x-1.0-alpha23 |
| Layout | Page Manager UI (page_manager_ui) | Not installed | 8.x-1.0-alpha23 |
| Layout | Panelizer (panelizer) | Enabled | 8.x-3.0-alpha2 |
| Lightning | Lightning Layout (lightning_layout) | Not installed | 8.x-1.03 |
| Lightning | Lightning Media (lightning_media) | Enabled | 8.x-1.03 |
| Lightning | Lightning Workflow (lightning_workflow) | Enabled | 8.x-1.03 |
| Lightning | Media Document (lightning_media_document) | Enabled | 8.x-1.03 |
| Lightning | Media Image (lightning_media_image) | Enabled | 8.x-1.03 |
| Lightning | Media Instagram (lightning_media_instagram) | Enabled | 8.x-1.03 |
| Lightning | Media Twitter (lightning_media_twitter) | Enabled | 8.x-1.03 |
| Lightning | Media Video (lightning_media_video) | Enabled | 8.x-1.03 |
| Media | Colorbox (colorbox) | Enabled | 8.x-1.1 |
| Media | dropzonejs (dropzonejs) | Enabled | 8.x-1.0-alpha2 |
| Media | DropzoneJS entity browser widget (dropzonejs_eb_widget) | Enabled | 8.x-1.0-alpha2 |
| Media | Entity Browser (entity_browser) | Enabled | 8.x-1.0-alpha7 |
| Media | Entity Browser example (entity_browser_example) | Not installed | 8.x-1.0-alpha7 |
| Media | Entity Browser IEF (entity_browser_entity_form) | Not installed | 8.x-1.0-alpha7 |
| Media | Media entity (media_entity) | Enabled | 8.x-1.3 |
| Media | Media entity document (media_entity_document) | Enabled | 8.x-1.1 |
| Media | Media entity image (media_entity_image) | Enabled | 8.x-1.2 |
| Media | Media entity Instagram (media_entity_instagram) | Enabled | 8.x-1.2 |
| Media | Media entity Twitter (media_entity_twitter) | Enabled | 8.x-1.2 |
| Migrate | Field Group Migrate (field_group_migrate) | Not installed | 8.1.7 |
| Migration | Migrate Example (migrate_example) | Not installed | 8.x-2.0-beta1 |
| Migration | Migrate Example (Advanced) (migrate_example_advanced) | Not installed | 8.x-2.0-beta1 |
| Migration | Migrate Plus (migrate_plus) | Enabled | 8.x-2.0-beta1 |
| Migration | Migrate Source CSV (migrate_source_csv) | Enabled | |
| Migration | Migrate Source JSON (migrate_source_json) | Enabled | |
| Migration | Migrate Tools (migrate_tools) | Enabled | 8.x-2.0-beta1 |
| Multilingual | Configuration Translation (config_translation) | Not installed | 8.1.7 |
| Multilingual | Content Translation (content_translation) | Not installed | 8.1.7 |
| Multilingual | Interface Translation (locale) | Not installed | 8.1.7 |
| Multilingual | Language (language) | Not installed | 8.1.7 |
| Other | CAS (cas) | Enabled | |
| Other | Configuration Update Base (config_update) | Enabled | 8.x-1.1 |
| Other | Configuration Update Reports (config_update_ui) | Not installed | 8.x-1.1 |
| Other | Embed (embed) | Enabled | 8.x-1.0-rc3 |
| Other | Entity (entity) | Enabled | 8.x-1.0-alpha3 |
| Other | File Browser (file_browser) | Enabled | 8.x-1.0-alpha1 |
| Other | File Browser Example (file_browser_example) | Not installed | 8.x-1.0-alpha1 |
| Other | Lightning (lightning_core) | Enabled | 8.x-1.03 |
| Other | Menu Link Attributes (menu_link_attributes) | Enabled | 8.x-1.0-beta3 |
| Other | Pathauto (pathauto) | Enabled | |
| Other | Redirect (redirect) | Not installed | 8.x-1.0-alpha1 |
| Other | Scheduled Updates (scheduled_updates) | Enabled | 8.x-1.0-alpha5 |
| Other | Token (token) | Enabled | 8.x-1.0-alpha2 |
| Other | Workbench moderation (workbench_moderation) | Enabled | 8.x-1.1 |
| Other | Yale CT Landing Page (yale_ct_landing_page) | Enabled | 8.x-1.0 |
| Panels | Panels (panels) | Enabled | 8.x-3.0-beta4 |
| Panels | Panels IPE (panels_ipe) | Enabled | 8.x-3.0-beta4 |
| Paragraphs | Paragraphs (paragraphs) | Enabled | 8.x-1.0 |
| Paragraphs | Paragraphs Type Permissions (paragraphs_type_permissions) | Not installed | 8.x-1.0 |
| Performance and scalability | Memcache (memcache) | Not installed | 8.x-2.0-alpha1 |
| Search | Database search (search_api_db) | Enabled | 8.x-1.0-alpha16 |
| Search | Database Search Defaults (search_api_db_defaults) | Not installed | 8.x-1.0-alpha16 |
| Search | Search API (search_api) | Enabled | 8.x-1.0-alpha16 |
| Search | Solr search (search_api_solr) | Not installed | 8.x-1.0-alpha4 |
| Security | Security Review (security_review) | Not installed | |
| SEO | Metatag (metatag) | Enabled | 8.x-1.0-beta9 |
| SEO | Metatag: Google Plus (metatag_google_plus) | Not installed | 8.x-1.0-beta9 |
| SEO | Metatag: Open Graph (metatag_open_graph) | Not installed | 8.x-1.0-beta9 |
| SEO | Metatag: Twitter Cards (metatag_twitter_cards) | Not installed | 8.x-1.0-beta9 |
| SEO | Metatag: Verification (metatag_verification) | Not installed | 8.x-1.0-beta9 |
| Sharing | ShareThis (sharethis) | Not installed | 8.x-2.0-beta3 |
| Statistics | Google Analytics (google_analytics) | Not installed | 8.x-2.0 |
| Video Embed Field | Video Embed Field (video_embed_field) | Enabled | 8.x-1.2 |
| Video Embed Field | Video Embed Media (video_embed_media) | Enabled | 8.x-1.2 |
| Video Embed Field | Video Embed WYSIWYG (video_embed_wysiwyg) | Not installed | 8.x-1.2 |
| Views | Views Infinite Scroll (views_infinite_scroll) | Enabled | 8.x-1.2 |
| Views | Views templates (views_templates) | Enabled | 8.x-1.0-alpha1 |
| Web services | HAL (hal) | Not installed | 8.1.7 |
| Web services | HTTP Basic Authentication (basic_auth) | Enabled | 8.1.7 |
| Web services | REST UI (restui) | Enabled | 8.x-1.11 |
| Web services | RESTful Web Services (rest) | Enabled | 8.1.7 |
| Web services | Serialization (serialization) | Enabled | 8.1.7 |
| XML sitemap | XML sitemap (xmlsitemap) | Enabled | 8.x-1.0-alpha2 |
| XML sitemap | XML sitemap custom (xmlsitemap_custom) | Not installed | 8.x-1.0-alpha2 |
| XML sitemap | XML sitemap engines (xmlsitemap_engines) | Not installed | 8.x-1.0-alpha2 |

##Custom modules
|*Package*|*Name*|*Status*|*Version*|
|------|--------------------------|--------|------|
| Yale | Yale Content Demo (yale_content_demo) | Enabled | |
| Yale | Yale CT Alert (yale_ct_alert) | Enabled | 8.x-1.0 |
| Yale | Yale CT Event (yale_ct_event) | Enabled | 8.x-1.0 |
| Yale | Yale CT Gallery (yale_ct_gallery) | Enabled | 8.x-1.0 |
| Yale | Yale CT News (yale_ct_news) | Enabled | 8.x-1.0 |
| Yale | Yale Global (yale_global) | Enabled | 8.x-1.0 |
| Yale | Yale Person (yale_ct_person) | Enabled | |
| Yale | Yale VC Events (yale_vc_events) | Enabled | 8.x-1.0 |
| Yale | Yale VC Person Type (yale_vc_person_type) | Enabled | 8.x-1.0 |


##Base theme
|*Package*|*Name*|*Status*|*Version*|
|------|--------------------------|--------|------|
| Core | Bartik (bartik) | Enabled | 8.1.7 |
| Core | Seven (seven) | Enabled | 8.1.7 |
| Core | Stark (stark) | Disabled | 8.1.7 |

##Custom themes
|*Package*|*Name*|*Status*|*Version*|
|------|--------------------------|--------|------|
| Other | Yale University (yale_foundation) | Enabled | |
| Other | ZURB Foundation 6 (zurb_foundation) | Enabled | |

#Content Architecture

##Alert
###An alert message which can be displayed at the top of the home page. Different alerts can be stored as draft nodes, but only one can be published at any one time.

|Field | Type                     |  Notes |
|------|--------------------------|--------|
|Body|body|Text (formatted, long, with summary)|

##Event
###Events are used to capture a calendar event. A calendar event can either be created manually or it can be automatically added from the Bedework Calendar application.

|Field | Type                     |  Notes |
|------|--------------------------|--------|
|Admission|field_event_admission|Text (plain, long)|
|Contact Email|field_event_email|Email|
|Contact Phone Number|field_event_phone|Telephone number|
|Date|field_event_date|Date|
|Event Description|field_event_description|Text (formatted, long)|
|Image|field_event_image|Image|
|Link to Original Event|field_event_original_link|Link|
|Open To|field_event_open_to|Text (plain, long)|
|Tags|field_event_tags|Entity reference|
|Title/Name of Contact|field_event_contact|Text (plain)|
|URL for related site|field_event_related_url|Link|
|Venue Name|field_event_location|Address|


##News
###The News content type is used to capture news articles.
|Field | Type                     |  Notes |
|------|--------------------------|--------|
|Body|body|Text (formatted, long, with summary)|
|Date|field_news_date|Date|
|External Link|field_news_external_link|Link|
|News Image|field_news_image|Image|

##Basic page
###Use basic pages for your static content, such as an 'About us' page.

|Field | Type                     |  Notes |
|------|--------------------------|--------|
|Body|body|Text (formatted, long, with summary)|
|field_meta_tags|Meta Tags||
|panelizer|Panelizer||

##Person
###An individual in the site that is not a Drupal user.
|Field | Type                     |  Notes |
|------|--------------------------|--------|
|Address|field_person_address|Address|
|Biography|field_person_biography|Text (formatted, long)|
|CV|field_person_cv|File|
|Degree Info|field_person_degree|Text (plain)|
|Department|field_person_department|Text (plain)|
|Division|field_person_division|Text (plain)|
|Email|field_person_email|Email|
|Fax|field_person_fax|Telephone number|
|First Name|field_person_first_name|Text (plain)|
|Last Name|field_person_last_name|Text (plain)|
|Person Type|field_person_type|Entity reference|
|Phone|field_person_phone|Telephone number|
|Picture|field_person_picture|Image|
|Title|field_person_title|Text (plain)|
|Website|field_person_website|Link|

##Gallery
###A gallery of slides.
|Field | Type                     |  Notes |
|------|--------------------------|--------|
|Body|body|Text (formatted, long, with summary)|
|Carousel|field_yale_gallery_carousel|Entity reference revisions|


##Landing Page
###A special page with its own one-off layout and content.
|Field | Type                     |  Notes |
|------|--------------------------|--------|
|Panelizer|panelizer|Panelizer|

##Events (vocabulary)
| Field       | Type                   | Notes                |
|-------------|------------------------|----------------------|
| Name        | Text                   |          -           |
| Description | Long text with summary |         -            |

##Person Type (vocabulary)
| Field       | Type                   | Notes                |
|-------------|------------------------|----------------------|
| Name        | Text                   |          -           |
| Description | Long text with summary |         -            |



